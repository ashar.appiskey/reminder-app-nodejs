const admin = require("firebase-admin");
const serviceAccount = require("../payment_reminder_nodejs_code/config/push-notifcations.json");
const cron = require("node-cron");

// Initialize the Firebase Admin SDK
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});

// Initialize Firestore
const db = admin.firestore();

const userIds = [];


// Function to send a notification to a user's device
function sendNotification(userId, token, title, body) {
  const message = {
    token: token,
    notification: {
      title: title,
      body: body,
    },
  };
  admin.messaging().send(message)
    .then((response) => {
      console.log("Successfully sent message:", response);
    })
    .catch((error) => {
      console.log("Error sending message:", error);
    });
}

// Cron job to send a notification
cron.schedule("*/30 * * * * *", async () => {
  await getuserIDs();
  userIds.forEach(async (ref) => {
    fbtokens(ref.id);
    // userIds.push(ref.id);
  });
  
});

 function fbtokens(userId){
    db.collection("users")
    .doc(userId)
    .get()
    .then((doc) => {
      if (doc.exists) {
        pushNotificationToUser(userId,doc.data()["firebaseToken"],"Payemnt Due","Some of your payments are due please check");  
      } else {
        console.log("No such document!");
      }
    })
    .catch((error) => {
      console.log("Error getting document:", error);
    });
}

function pushNotificationToUser(userId,token,title,body){
   
    sendNotification(userId, token, title, body);
}

 async function getuserIDs(){
   
// Retrieve all user IDs from the "users" collection
const userRefs = db.collection("users").listDocuments();

userRefs.then((refs) => {
  refs.forEach(async (ref) => {
    fbtokens(ref.id);
    userIds.push(ref.id);
  });
  console.log(userIds);
})
.catch((error) => {
  console.log("Error getting documents:", error);
});

}
